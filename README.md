# RMarkdown-Word-HTML-example

Example project which is built and published automatically: <https://rikh.gitlab.io/rmarkdown-word-html-example>.

This project contains an example R Markdown project.
When a change is pushed to this repository, then the HTML webpage and Word file are automatically generated.
Next, the generated files are published to a GitLab (Pages) website at <https://rikh.gitlab.io/rmarkdown-word-html-example>.
Using GitLab's servers to build, or specifically knit, has multiple benefits:

- the build environment is explicit, which aids reproducibility,
- it ensures that the HTML and Word file correspond to the current state of the code,
- the Git diff shows only manually applied changes and not the changes in the generated artifacts,
- the repository size is smaller since it does not contain the generated artifacts.

## Using this example

To use this example project, simply copy or fork the files to your own GitLab repository.
GitLab will read the `.gitlab-ci.yml` file and start processing.
By default, the website will be available at `https://<GitLab username>.gitlab.io/<project name>`.

## Securing the website with a password

It is possible to secure the website with a password.
To do so, go to **Settings -> General** and set the visibility of **Pages** to "Only project members". 
Then, you can add GitLab users to the project to give them access.
Alternatively, you can create a new GitLab account, provide the account with viewing access and share that account with others.

## Building locally

The automated build runs `Rscript knit.R` to generate both the HTML and Word files.
To work locally, knit `analysis.Rmd` from RStudio or run `Rscript knit.R` from a terminal.

## Automated build and publication

Automatically building and publishing is achieved via [GitLab CI/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/).
The project's static Pages are built according to the steps defined in `.gitlab-ci.yml`. 
The build image is based on the [Nix package manager](https://nixos.org/nix/) for improved reliability and reproducibility.
The build environment is specified in `default.nix`.
